### hefring.aegirproject.org

This repo is hosting the code for the IRC bot called hefring

Module used: https://www.drupal.org/project/bot

For issues please use the [hostmaster queue](https://www.drupal.org/project/issues/hostmaster)

### Setup

This requires the Net/SmartIRC php class, found in the php-net-smartirc debian package.

It's good to have a monitoring check to see if the bot is still online. A simple example for NRPE:
```
command[check_hefring]=/usr/lib/nagios/plugins/check_procs -c 1: -a bot-start
```


Cron: /etc/cron.d/bot-check-cron
```
#m h  dom mon dow   command
42 * * * *       aegir   /usr/local/bin/bot-check

@reboot aegir   /usr/local/bin/bot-check
```

/usr/local/bin/bot-check
```
#!/bin/sh

#BOT=hefring.mig5.net
BOT=hefring.aegirproject.org

if pgrep -f "bot-start" > /dev/null
  then
    exit 0
  else
    drush cc drush;
    drush --yes @$BOT bot-status-reset
    sleep 5
    nohup drush -y @$BOT bot-start &
fi

```

### Start


drush  @hefring.aegirproject.org bot-start &
drush  @hefring.aegirproject.org bot-status-reset --yes
